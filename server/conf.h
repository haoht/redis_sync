#ifndef CONF_H
#define CONF_H 1

#include <unistd.h>

#ifdef __cplusplus
extern "C" {
#endif


/**
 * [config_init description]
 * @param  config_file_path [description]
 * @return                  [0 means success, -1 means failed]
 */
int config_init(const char * config_file_path);

/**
 * [get_config_item description]
 * @param  config_item_name [description]
 * @param  result           [description]
 * @param  len              [description]
 * @param  default_value    [description]
 * @return                  [0 means success, -1 means failed]
 */
int get_config_item(const char * config_item_name, char * result, size_t len, const char * default_value);

/**
 * [get_config_item_int description]
 * @param  config_item_name [description]
 * @param  default_value    [description]
 * @return                  [real value or default value]
 */
int get_config_item_int(const char * config_item_name, int default_value);


#ifdef __cplusplus
}
#endif

#endif /*CONF_H*/